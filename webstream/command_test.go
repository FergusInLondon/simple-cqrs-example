package webstream

import (
	"bytes"
	"context"
	"example/dispatcher"
	"testing"
)

func Test_CommandShouldCallUpdateForConnectedWebstreamClients(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	Start(ctx)
	defer cancel()

	payload := []byte("shouldupdate")
	conn := NewConnection()
	done := make(chan struct{})

	go func() {
		p := <-conn.Payload
		if bytes.Compare(p, payload) != 0 {
			t.Fail()
		}

		done <- struct{}{}
	}()

	cmd := &UpdateWebstreamClientsCommand{
		Payload: payload,
	}
	dispatcher.Dispatch(cmd)

	<-done
}
