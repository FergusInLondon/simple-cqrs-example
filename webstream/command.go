package webstream

import "example/dispatcher"

// UpdateWebstreamClientsCommand is a dispatchable command that is responsible
// for sending new payloads to any connected webstream clients.
type UpdateWebstreamClientsCommand struct {
	Payload []byte
}

// Do is to provide adherence to the expected interface by the Dispatcher; all
// it does is proxy the Payload to `Update([]byte)`.
func (cmd *UpdateWebstreamClientsCommand) Do(_ *dispatcher.DispatchableConfig) error {
	return Update(cmd.Payload)
}
