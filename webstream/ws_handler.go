package webstream

import (
	"example/dispatcher"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

// WebsocketHandler returns a http.HandlerFunc that is responsible for handling
// inbound websocket connections, dispatching updates to any connected clients.
func WebsocketHandler(_ dispatcher.DispatchFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			panic(err)
		}

		conn := NewConnection()
		defer func() {
			conn.Close()
			ws.Close()
		}()

		for {
			select {
			case <-conn.Stop:
				return
			case message, ok := <-conn.Payload:
				if !ok {
					ws.WriteMessage(websocket.CloseMessage, []byte{})
					return
				}

				if err := ws.WriteMessage(websocket.TextMessage, message); err != nil {
					ws.WriteMessage(websocket.CloseAbnormalClosure, nil)
					return
				}
			}
		}
	}
}
