package webstream

import (
	"bytes"
	"context"
	"strings"
	"testing"
)

func Test_NewConnectionsShouldHaveUniqueUUIDs(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	Start(ctx)
	defer cancel()

	existingIds := make([]string, 0)
	for i := 0; i < 5; i++ {
		conn := NewConnection()
		defer conn.Close()

		for _, id := range existingIds {
			if strings.Compare(conn.uuid, id) == 0 {
				t.Fail()
			}
		}

		existingIds = append(existingIds, conn.uuid)
	}
}

func Test_NewConnectionsShouldBeThreadSafe(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	Start(ctx)
	defer cancel()

	concurrentWrites := 5
	individualWrites := 5

	doneChan := make(chan struct{})
	defer close(doneChan)

	for i := 0; i < concurrentWrites; i++ {
		go func() {
			for j := 0; j < individualWrites; j++ {
				NewConnection()
			}

			doneChan <- struct{}{}
		}()
	}

	for i := 0; i < concurrentWrites; i++ {
		<-doneChan
	}

	if len(connections) != (concurrentWrites * individualWrites) {
		t.Fail()
	}
}

func Test_CloseShouldRemoveConnectionFromRegistry(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	Start(ctx)
	defer cancel()

	conn := NewConnection()
	if _, exists := connections[conn.uuid]; !exists {
		t.Fail()
	}

	conn.Close()
	if _, exists := connections[conn.uuid]; exists {
		t.Fail()
	}
}

func Test_UpdateShouldDistributePayloadsAmongstAllConnections(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	Start(ctx)
	defer cancel()

	payload := []byte("helloworld")
	conns := make([]*Connection, 0)
	receivedFrom := make(chan int)

	for i := 0; i < 5; i++ {
		conns = append(conns, NewConnection())
		go func(i int) {
			p := <-conns[i].Payload
			if bytes.Compare(p, payload) != 0 {
				t.Fail()
			}

			receivedFrom <- i
		}(i)
	}

	if err := Update(payload); err != nil {
		t.Fail()
	}

	for i := 0; i < 5; i++ {
		toClose := <-receivedFrom
		conns[toClose].Close()
	}
}

func Test_UpdateShouldErrorWhenManagerIsInactive(t *testing.T) {
	if err := Update([]byte("thisshouldfail")); err != ErrorConnectionManagerNotActive {
		t.Fail()
	}
}
