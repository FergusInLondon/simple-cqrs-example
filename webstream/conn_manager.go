package webstream

import (
	"context"
	"errors"
	"sync"

	"github.com/google/uuid"
)

var (
	connections    map[string]*Connection = make(map[string]*Connection)
	connectionsMtx *sync.RWMutex          = &sync.RWMutex{}
	isActive       bool
	toSend         chan []byte = make(chan []byte)
)

// Connection represents a client connected to the server over a websocket;
// it contains two channels: one for signifying a "stop" event, and one for
// outbound payloads to dispatch to the client.
type Connection struct {
	uuid    string
	Stop    chan struct{}
	Payload chan []byte
}

// NewConnection generates a new Connection struct, complete with unique ID.
// It then adds a pointer to this Connection to the `connections` map.
func NewConnection() *Connection {
	conn := &Connection{
		uuid:    uuid.New().String(),
		Payload: make(chan []byte),
	}

	connectionsMtx.Lock()
	connections[conn.uuid] = conn
	connectionsMtx.Unlock()

	return conn
}

// Close removes the connection from the map, and then closes the "stop" channel,
// ensuring that the websocket handler is aware that it's time to disconnect.
func (c *Connection) Close() {
	connectionsMtx.Lock()
	delete(connections, c.uuid)
	connectionsMtx.Unlock()

	close(c.Stop)
}

// Start runs in the background monitoring for any new messages that have
// been accepted via `Update()`.
func Start(ctx context.Context) {
	done := make(chan struct{})

	go func(c context.Context) {
		isActive = true
		close(done)
		for {
			select {
			case <-ctx.Done():
				isActive = false
				return
			case payload := <-toSend:
				connectionsMtx.RLock()
				for _, conn := range connections {
					conn.Payload <- payload
				}
				connectionsMtx.RUnlock()
			}
		}
	}(ctx)
	<-done
}

// ErrorConnectionManagerNotActive is returned in the event that `Update`
// is called, but `Start()` is still yet to happen. Although this can be
// ignored, it's likely symptomatic of a larger problem.
var ErrorConnectionManagerNotActive = errors.New("received update but not currently handling stream connections")

// Updater is the signature required for the function responsivle for sending
// new payloads to connected clients. By default this is `Update([]byte)`; but
// this type definition is quite handy for testing.
type Updater func([]byte) error

// Update checks that we're handling incoming stream connections correctly, and
// then forwards the payload on - where it will be fanned out to all connections.
func Update(payload []byte) error {
	if !isActive {
		return ErrorConnectionManagerNotActive
	}

	toSend <- payload
	return nil
}
