package message

import (
	"example/dispatcher"
)

// ListingQuery is the Query for retrieving Message records from the database.
// It currently lacks pagination or filtering.
type ListingQuery struct {
	Results []Message
}

// Get executes the query required to get *all* Message records from the database,
// subsequently populating the `Results` property of the Query struct.
func (c *ListingQuery) Get(cfg *dispatcher.DispatchableConfig) error {
	c.Results = []Message{}
	cfg.DatabaseConn.Find(&c.Results)
	return nil
}
