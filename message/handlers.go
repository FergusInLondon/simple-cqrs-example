package message

import (
	"encoding/json"
	"example/dispatcher"
	"io/ioutil"
	"net/http"
)

// HTTPHandlers returns a handler for a very simplistic kinda-RESTFul route; in
// addition to GET requests (for listing), POST (create), PUT (update), and
// DELETE (delete) are all supported.
func HTTPHandlers(dispatch dispatcher.DispatchFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			getMessages(dispatch, w, r)
			return
		}

		var (
			command dispatcher.Command
			body    []byte
			err     error
		)

		switch r.Method {
		case http.MethodPost:
			command = &CreateMessage{}
		case http.MethodPut:
			command = &UpdateMessage{}
		case http.MethodDelete:
			command = &DeleteMessage{}
		default:
			w.WriteHeader(http.StatusNotImplemented)
			return
		}

		if body, err = ioutil.ReadAll(r.Body); err == nil {
			err = json.Unmarshal(body, command)
		}

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		dispatch(command)
		w.WriteHeader(http.StatusOK)
	}
}

func getMessages(dispatch dispatcher.DispatchFunc, w http.ResponseWriter, r *http.Request) {
	query := &ListingQuery{}
	if err := dispatch(query); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	body, err := json.Marshal(query.Results)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write(body)
}
