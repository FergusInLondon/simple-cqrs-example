package message

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const examplePayload = `{ "validation" : "as if!" }`

func TestMessageHandlerAllowedMethods(t *testing.T) {
	handler := HTTPHandlers(func(_ interface{}) error {
		return nil
	})

	testCases := map[string]int{
		http.MethodGet:    http.StatusOK,
		http.MethodPost:   http.StatusOK,
		http.MethodPut:    http.StatusOK,
		http.MethodPatch:  http.StatusNotImplemented,
		http.MethodDelete: http.StatusOK,
	}

	for method, expectedStatusCode := range testCases {
		req, err := http.NewRequest(method, "/messages", strings.NewReader(examplePayload))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)

		actualStatusCode := rr.Result().StatusCode
		if actualStatusCode != expectedStatusCode {
			t.Errorf("incorrect status code returned by audit endpoint (%s:%d)",
				method, actualStatusCode)
		}
	}
}

func TestMessageHandlerGeneratesCorrectCommandOrQuery(t *testing.T) {
	testCases := map[string]interface{}{
		http.MethodGet:    &ListingQuery{},
		http.MethodPost:   &CreateMessage{},
		http.MethodPut:    &UpdateMessage{},
		http.MethodDelete: &DeleteMessage{},
	}

	for method, expectedDispatchable := range testCases {
		req, err := http.NewRequest(method, "/messages", strings.NewReader(examplePayload))
		if err != nil {
			t.Fatal(err)
		}

		handler := HTTPHandlers(func(d interface{}) error {
			actualType := reflect.TypeOf(d)
			expectedType := reflect.TypeOf(expectedDispatchable)

			if actualType != expectedType {
				t.Errorf("incorrect command or query generated for request!")
			}

			return nil
		})

		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)

		if rr.Result().StatusCode != http.StatusOK {
			t.Errorf("incorrect status code returned by audit endpoint (%s:%d)",
				method, rr.Result().StatusCode)
		}
	}
}
