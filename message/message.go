package message

import (
	"github.com/jinzhu/gorm"
)

// Message is the database model for a Message record; it's very simple and only
// posseses a `title` and `message` field.
type Message struct {
	gorm.Model
	Title   string `json:"title"`
	Message string `json:"message"`
}

// TableName allows gorm to utilise the correct database table, even when the model
// is embedded in another struct.
func (ae *Message) TableName() string {
	return "messages"
}
