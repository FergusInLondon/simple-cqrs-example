package message

import (
	"errors"
	"example/audit"
	"example/dispatcher"
)

// CreateMessage is a Command, it embeds the Message model, and has the
// attached behaviour of creating a new record.
type CreateMessage struct {
	Message
}

// Do checks that the Message record is not currently in the database - i.e
// it doesn't have an ID - and then creates it. A corresponding Audit entry
// is also created.
func (c *CreateMessage) Do(cfg *dispatcher.DispatchableConfig) error {
	if !cfg.DatabaseConn.NewRecord(c) {
		return errors.New("duplicate record")
	}

	cfg.DatabaseConn.Create(c)

	auditEntry := &audit.CreateEntry{}
	auditEntry.User = "DummyUser"
	auditEntry.ActionType = audit.AuditActionCreate
	cfg.Dispatch(auditEntry)

	return nil
}

// UpdateMessage is a Command, it embeds the Message model, and has the
// attached behaviour of updating an existing record.
type UpdateMessage struct {
	Message
}

// Do updates the corresponding database record, before creating the applicable
// audit record.
func (c *UpdateMessage) Do(cfg *dispatcher.DispatchableConfig) error {
	cfg.DatabaseConn.Update(c)

	auditEntry := &audit.CreateEntry{}
	auditEntry.User = "DummyUser"
	auditEntry.ActionType = audit.AuditActionCreate
	cfg.Dispatch(auditEntry)

	return nil
}

// DeleteMessage is a Command, it embeds the Message model - although only
// requires the primary key ("ID") database record - and has the attached
// behaviour of deleting an existing record.
type DeleteMessage struct {
	Message
}

// Do deletes the message record from the database; like other mutations, this
// triggers the creation of an audit record.
func (c *DeleteMessage) Do(cfg *dispatcher.DispatchableConfig) error {
	cfg.DatabaseConn.Delete(c)

	auditEntry := &audit.CreateEntry{}
	auditEntry.User = "DummyUser"
	auditEntry.ActionType = audit.AuditActionDelete
	cfg.Dispatch(auditEntry)

	return nil
}
