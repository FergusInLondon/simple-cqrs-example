package audit

import (
	"example/dispatcher"
)

// ListingQuery is a Query entity for retrieving all queries from the databse,
// note that the struct simply acts as a wrapper around a "Result" property - i.e
// a slice of AuditEntry structs.
type ListingQuery struct {
	Results []Entry
}

// Get is the implementation required for the Query interface, note that there's
// no pagination - parameters for things like this would be passed in via the
// Query struct - i.e ListingQuery.
func (c *ListingQuery) Get(cfg *dispatcher.DispatchableConfig) error {
	c.Results = []Entry{}
	cfg.DatabaseConn.Find(&c.Results)
	return nil
}
