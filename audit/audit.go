package audit

import (
	"time"
)

const (
	// AuditActionCreate signifies an operation where the user creates a new record
	AuditActionCreate = "CREATE"
	// AuditActionUpdate signifies an operation where the user updates a new record
	AuditActionUpdate = "UPDATE"
	// AuditActionDelete signifies an operation where the user deletes a new record.
	// note: deletions are *soft*, and records remain in database.
	AuditActionDelete = "DELETE"
)

// Entry contains the information required for an Audit Log Entry;
// notably (a) the user who performed the action, (b) the type of action
// that the user performed, and (c) when the action was performed.
type Entry struct {
	ID         uint `gorm:"primary_key"`
	User       string
	ActionType string
	Time       time.Time
}

// TableName is used by gorm to determine the database table to be used
// for read and write operations.
func (ae *Entry) TableName() string {
	return "audit"
}
