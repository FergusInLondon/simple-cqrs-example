package audit

import (
	"example/dispatcher"
	"encoding/json"
	"net/http"
)

// HTTPHandlers returns a handler for all Audit related routes, these are currently
// limited to GET requests; there is reason that an external system should be able
// to mutate our audit log.
func HTTPHandlers(dispatch dispatcher.DispatchFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusNotImplemented)
			return
		}

		query := &ListingQuery{}
		if err := dispatch(query); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		body, err := json.Marshal(query.Results)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Write(body)
		return
	}
}
