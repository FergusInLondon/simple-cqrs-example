package audit

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAuditHandlerAllowedMethods(t *testing.T) {
	handler := HTTPHandlers(func(_ interface{}) error {
		return nil
	})

	testCases := map[string]int{
		http.MethodGet:    http.StatusOK,
		http.MethodPost:   http.StatusNotImplemented,
		http.MethodPut:    http.StatusNotImplemented,
		http.MethodPatch:  http.StatusNotImplemented,
		http.MethodDelete: http.StatusNotImplemented,
	}

	for method, expectedStatusCode := range testCases {
		req, err := http.NewRequest(method, "/audit", nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)

		actualStatusCode := rr.Result().StatusCode
		if actualStatusCode != expectedStatusCode {
			t.Errorf("incorrect status code returned by audit endpoint (%s:%d)",
				method, actualStatusCode)
		}
	}
}
