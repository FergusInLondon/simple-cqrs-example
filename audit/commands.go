package audit

import (
	"errors"
	"example/dispatcher"
	"time"
)

// CreateEntry is used to update the Audit table with a new record.
type CreateEntry struct {
	Entry
}

// Do is the Command implementation, and persists the AuditEntry to the database.
// note: AuditEntry doesn't have the default gorm fields, and therefore we manually
// create the timestamp.
func (c *CreateEntry) Do(cfg *dispatcher.DispatchableConfig) error {
	c.Time = time.Now()
	if !cfg.DatabaseConn.NewRecord(c) {
		return errors.New("duplicate record")
	}

	cfg.DatabaseConn.Create(c)
	return nil
}
