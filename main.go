package main

import (
	"context"
	"example/audit"
	"example/dispatcher"
	"example/message"
	"example/webstream"

	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	db.AutoMigrate(&message.Message{}, &audit.Entry{})
	dispatcher.Database = db

	ctx, cancel := context.WithCancel(context.Background())
	webstream.Start(ctx)
	defer cancel()

	http.HandleFunc("/audit", audit.HTTPHandlers(dispatcher.Dispatch))
	http.HandleFunc("/messages", message.HTTPHandlers(dispatcher.Dispatch))
	http.HandleFunc("/ws/webstream", webstream.WebsocketHandler(dispatcher.Dispatch))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
