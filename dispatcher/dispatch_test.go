package dispatcher

import "testing"

type validCommand struct {
	t      *testing.T
	called bool
}

func (vc *validCommand) Do(cfg *DispatchableConfig) error {
	if cfg.Dispatch == nil {
		vc.t.Errorf("invalid config provided to command")
	}
	vc.called = true
	return nil
}

func Test_DispatcherWillCallDoOnCommand(t *testing.T) {
	command := &validCommand{t, false}
	err := Dispatch(command)

	if err != nil {
		t.Errorf("encountered unexpected error!")
	}

	if !command.called {
		t.Errorf("dispatcher failed to call Do() on Command")
	}
}

type validQuery struct {
	t      *testing.T
	called bool
}

func (vc *validQuery) Do(cfg *DispatchableConfig) error {
	if cfg.Dispatch == nil {
		vc.t.Errorf("invalid config provided to query")
	}
	vc.called = true
	return nil
}

func Test_DispatcherWillCallGetOnQuery(t *testing.T) {
	query := &validQuery{t, false}
	err := Dispatch(query)

	if err != nil {
		t.Errorf("encountered unexpected error!")
	}

	if !query.called {
		t.Errorf("dispatcher failed to call Do() on Command")
	}
}

type invalidInput struct{}

func Test_DispatcherWillErrorOnInvalidInput(t *testing.T) {
	invalid := &invalidInput{}
	err := Dispatch(invalid)

	if err != ErrorInvalidCommandOrQuery {
		t.Errorf("expected ErrorInvalidCommandOrQuery, got %s", err)
	}
}
