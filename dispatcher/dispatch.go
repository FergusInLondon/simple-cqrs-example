package dispatcher

import (
	"errors"

	"github.com/jinzhu/gorm"
)

var (
	// Database contains the gorm database access struct, it's used to be injected via
	// the use of DispatchableConfig
	Database *gorm.DB
	// ErrorInvalidCommandOrQuery is returned when Dispatch is given an invalid parameter
	ErrorInvalidCommandOrQuery = errors.New("supplied struct is neither a command or a query")
)

// DispatchFunc is a type definition for the `Dispatch(...)` function contained in this
// package; it's also relied upon for testability of dependent code.
type DispatchFunc func(interface{}) error

// DispatchableConfig is used for dependency injection for Commands and Queries. at the
// moment it's very simple and only wraps around the ORM.
type DispatchableConfig struct {
	DatabaseConn *gorm.DB
	Dispatch     DispatchFunc
}

// Command is a simple interface that requires a `Do(...)` method which returns an error.
type Command interface {
	Do(*DispatchableConfig) error
}

// Query is a simple interface that requires a `Do(...)` method which returns an error.
type Query interface {
	Get(*DispatchableConfig) error
}

// Dispatch accepts a "Dispatchable" - that is, a Command or Query - and executes it. In
// the event of the parameter not being a valid dispatchable, an error is returned.
func Dispatch(dispatchable interface{}) error {
	cfg := &DispatchableConfig{Database, Dispatch}

	if cmd, isCommand := dispatchable.(Command); isCommand {
		return cmd.Do(cfg)
	}

	if query, isQuery := dispatchable.(Query); isQuery {
		return query.Get(cfg)
	}

	return ErrorInvalidCommandOrQuery
}
