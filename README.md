
# Super simple CQRS example

This is a - pretty contrived - example of a very simple CQRS mechanism. It was written entirely for a blog post, and should not be taken as anything more. Notably it has no unit testing, linting, or comments!

It has iterations - from incredibly naive to the `master` one - available in the tags `v1`, `v2`, and `v3`.

**[This accompanies a blog post available at blog.fergus.london](https://blog.fergus.london/code/simplest-cqrs-implementation-possible/)**